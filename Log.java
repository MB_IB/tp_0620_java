import java.sql.*;

public class Log {
    public static void main(String[]args) throws Exception {
    
        Thread.sleep(5000); // attendre un peu le serveur MySQL
    
        // Connexion
        Class.forName("com.mysql.jdbc.Driver");
        Connection connect = DriverManager.getConnection(
            "jdbc:mysql://localhost/labase&user=user&password=1234");
 
        PreparedStatement ps1 = connect
            .prepareStatement("CREATE TABLE log(texte VARCHAR(100));");
        ps1.executeUpdate();
 
        PreparedStatement ps2 = connect
            .prepareStatement("insert into log VALUES(?)");
        ps2.setString(1, "ici");
        ps2.executeUpdate();
        
        // Deconnexion
        connect.close();
        System.out.println("Ended");
    }
}